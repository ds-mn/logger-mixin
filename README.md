## Logger Mixin
[![pipeline status](https://gitlab.com/ds-mpca/logger-mixin/badges/master/pipeline.svg)](https://gitlab.com/ds-mpca/logger-mixin/-/commits/master)[![coverage report](https://gitlab.com/ds-mpca/logger-mixin/badges/master/coverage.svg)](https://gitlab.com/ds-mpca/logger-mixin/-/commits/master)

[![PyPI](https://img.shields.io/pypi/v/logger-mixin?style=flat-square)![PyPI - Python Version](https://img.shields.io/pypi/pyversions/logger-mixin?style=flat-square)![PyPI - License](https://img.shields.io/pypi/l/logger-mixin?style=flat-square)](https://pypi.org/project/logger-mixin/)
